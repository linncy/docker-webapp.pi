FROM ubuntu:18.04
MAINTAINER Changyuan

ENV DEBIAN_FRONTEND noninteractive
ENV TOMCAT tomcat7

# install Java, Tomcat, supervisor
RUN sudo add-apt-repository ppa:webupd8team/java \
&& apt-get -y update \
&& echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
&& apt-get -y install oracle-java8-installer oracle-java8-set-default \
&& apt-get -y install ${TOMCAT} \
&& apt-get -y install supervisor \
&& rm -rf /var/lib/apt/lists/* \
&& apt-get clean

# copy the script that starts Tomcat
COPY scripts/start-tomcat.sh /usr/bin/start-tomcat.sh

# Supervisor config
COPY supervisor/tomcat.conf /etc/supervisor/conf.d/

# copy the π webapp
COPY webapps/pi.war /var/lib/${TOMCAT}/webapps/pi.war

### Tomcat ports
# 8080: default website
EXPOSE 8080

# The main process of this container is supervisor
CMD ["supervisord", "-n"]
